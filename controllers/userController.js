const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {

		if (result.length > 0) {
			return true;
		} else {
			return false;
		};
	});
};

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password,10)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true;
		};
	});
};

module.exports.loginUser = (reqBody) => {

	// We use the "findOne" method instead of the "find" method which return all records that match the search criteria
	// The "findOne" method returns teh first record in the collection that matches teh serach criteria
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false;
		}else {

			// the "compareSync" method is used to compare a non encrypted password from teh login from to the encrypted password retrieved from the database and it returns "true" or "false" value depending on the result
			// A good coding practice for boolean variable/constants is to use teh word "is" or "are" at the beginning in teh form of is+Noun
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect){
				return { access : auth.createAccessToken(result)}
			}else {
				return false;
			}
		}
	})
}

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		result.password = "";
		return result;
	});
};
